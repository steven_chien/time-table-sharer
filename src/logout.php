<!--
Copyright (C) 2014 Steven Chien, Troy Lenger and Nikita Ko

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->

<?php
	//set expire time of the cookie for the site to the past for the cookies to become invalid
	setcookie('time_table_sharer_login', false, time()-3600);
	setcookie('user_name', 0, time()-3600);
	setcookie('name', 0, time()-3600);

	//redirect to login page
	header("Location:index.php");
?>
