<!DOCTYPE html>
<!--
Copyright (C) 2014 Steven Chien, Troy Lenger and Nikita Ko

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->
<?php
	//setup database connection and check login status and setup environment variable
	//include("check_login.php");
	include("database.php");

	if(isset($_POST['submit'])) {
		extract($_POST);
		$stmt = $mysql->prepare("insert into account values(?,?,?,sha1(?));");
		$stmt->execute(array($email,$nickName,$fullName,$password));
		header("Location:index.php");
	}
?>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="index.css"></link>
	<title>New Account - Time Table</title>
</head>
<body>
<table>
	<form name="application" method="post" action="new_account.php">
	<tr><td>Full Name</td><td><input type="text" name="fullName" required></td></tr>
	<tr><td>Nick Name</td><td><input type="text" name="nickName" required></td></tr>
	<tr><td>Email</td><td><input type="email" name="email" required></td></td></tr>
	<tr><td>Password</td><td><input type="password" name="password" required></td></tr>
	<tr><td colspan="2"><input class="myButton" type="submit" value="submit" name="submit"></td></tr>
	</form>
</table>
</body>
</html>
