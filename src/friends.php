<!DOCTYPE html>
<!--
Copyright (C) 2014 Steven Chien, Troy Lenger and Nikita Ko

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->

<?php
	//setup database connection and check login status and extract environment variables
	include("check_login.php");
	include("database.php");
	extract($_COOKIE);
	extract($_POST);

	//check if requested ID is added to friend list
	$query = "select friend_id from friends where UID=? and friend_id=?";
	$stmt = $mysql->prepare($query);
	$stmt->execute(array($user_name,$friend));
	$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

	//if not exist check if user exist i.e. if the ID is correct
	$row_count = count($rows);
	if($row_count==0) {
		//add friend and redirect
		//check if user exist
		$query = "select * from account where email=?;";
		$stmt = $mysql->prepare($query);
		$stmt->execute(array($friend));
		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

		//if requested ID exist, establish friend relationship
		if(count($rows)>0) {
			//insert into friend table
			$query = "insert into friends values(?,?);";
			$stmt = $mysql->prepare($query);
			$stmt->execute(array($user_name,$friend));

			//redirect to friend's timetable
			header("Location:time_table.php?id=".$friend);
		}
		else {
			//if friend not found, notify user
			echo '<html>';
			echo '<head><link rel="stylesheet" type="text/css" href="index.css"></link></head>';
			echo '<body>';
			echo '<p>user ID not found</p>';
			echo '<a class="myButton" href="time_table.php">Go Back</a>';
			echo '</body>';
			echo '</html>';
		}
	}
?>