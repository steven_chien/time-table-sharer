<!--
Copyright (C) 2014 Steven Chien, Troy Lenger and Nikita Ko

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->

<?php
	include("database.php");
	extract($_POST);

	try {	

		$query = "select name from account where email=? and password=sha1(?);";
		$exec = $mysql->prepare($query);
		$exec->execute(array($Email,$Password));

		$exec->setFetchMode(PDO::FETCH_ASSOC);
		$row = $exec->fetch();
	}
	catch(PDOException $e) {
		echo $e->getMessage();
	}

	if(!empty($row['name'])) {
		setcookie('time_table_sharer_login', true, time()+3600);
		setcookie('user_name', $Email, time()+3600);
		setcookie('name', $row['name'], time()+3600);
		header("Location:time_table.php");
	}
	else {
		header("Location:index.php");
	}
?>
