<!--
Copyright (C) 2014 Steven Chien, Troy Lenger and Nikita Ko

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->

<?php
	include("check_login.php");
	include("database.php");

	if(isset($_GET['id'])) {
		extract($_GET);
		$query = "select name, start_time, end_time, location, day_of_week from time_table where event_id=?";
		$stmt = $mysql->prepare($query);
		$stmt->execute(array($id));
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
	}
	else {
		die("id not set");
	}
?>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="index.css">
	<title>Event Details - Time Table Viewer</title>
</head>
<body>
	<table>
		<tr><td>Name</td><td><?php echo $row['name']; ?></td></tr>
		<tr><td>Start</td><td><?php echo $row['start_time']; ?></td></tr>
		<tr><td>End</td><td><?php echo $row['end_time']; ?></td></tr>
		<tr><td>Location</td><td><?php echo $row['location']; ?></td></tr>
	</table>
	<input class="myButton" type="button" onclick="window.close();" value="close"></input><a class="myButton" href="delete.php?id=<?php echo $id; ?>">delete</a>
</body>
</html>