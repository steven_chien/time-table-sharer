<!DOCTYPE html>
<!--
Copyright (C) 2014 Steven Chien, Troy Lenger and Nikita Ko

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->
<?php
	//setup database connection and check login status and setup environment variable
	include("check_login.php");
	include("database.php");
	extract($_COOKIE);
	include("schedule.php");

	//if user id is set in URL, extract $_GET
	if(isset($_GET['id'])) {

		extract($_GET);

		//check if user ID exists in database
		$query = "select full_name from account where email=?";
		$stmt = $mysql->prepare($query);
		$stmt->execute(array($id));
		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

		//if user exists, set the user_id for this time as the requested ID
		if(count($rows)>0) {
			//user exist
			$user_id = $id;
			$owner_name = $rows[0]['full_name'];
		}
		//if user does not exists, set this time as the login user
		else {
			$user_id = $user_name;
		}
	}
	else {
		//user id is not set in url, set user ID for this time as current user
		$user_id = $user_name;
		$owner_name = $name;
	}

	//load friend list into a selection/drop down list for quick access
	$query = "select friends.friend_id, account.full_name from friends, account where friends.friend_id=account.email and friends.UID=?;";
	$stmt = $mysql->prepare($query);
	$stmt->execute(array($user_name));

	//setup html page
	$list = '<select onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);">';
	$list .= '<option value="">-select a user-</option>';
	$list .= '<option value="time_table.php">Yourself</option>';
	while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
		$list .= "<option value=\"time_table.php?id=".$row['friend_id']."\">".$row['full_name']."</option>";
	}
	$list .= '</select>';
?>

<html>
	<head>
		<title>home - Time Table Viewer</title>
		<!-- <script src="js/jquery-2.1.0.js" type="text/javascript"></script> //jQuery for later use-->
        <style type = "text/css">
			td.border_bottom {
                border-bottom: 1pt solid white;
			}
		</style>
		<link rel="stylesheet" type="text/css" href="index.css"></link>
	</head>
	<body>
		<h1>Welcome back! <?php echo $name; /* show user name */ ?></h1>
		<h4>
			Select a timetable to view <?php echo $list; /* show drop down list of friends */?>
		<h4>
			<form name="friend_search" method="post" action="friends.php">
				Find a friend: <input type="text" name="friend">
				<input class="myButton" type="submit" name="submit" value="search">
			</form>
		</h4>

		<i>You are viewing <?php echo $owner_name; /* owner of the loaded time table */ ?>'s timetable</i><br></br>

		<?php
			//setup and format time table
			$table = "<table id=\"time_table\" width=100% cellspacing=0 cellpadding=0>";
			$table .= "<tr> <td width=5%></td> 
						<td style='word-wrap:break-word;width:12%;'>Sun</td> 
						<td style='word-wrap:break-word;width:12%;'>Mon</td> 
						<td style='word-wrap:break-word;width:12%;'>Tue</td> 
						<td style='word-wrap:break-word;width:12%;'>Wed</td> 
						<td style='word-wrap:break-word;width:12%;'>Thur</td> 
						<td style='word-wrap:break-word;width:12%;'>Fri</td> 
						<td style='word-wrap:break-word;width:12%;'>Sat</td> </tr>";

			// 18 hours starting from 6am
			for($i=0; $i<18; $i++) {
				//one hour has first half and second half 
				for($j=0; $j<2; $j++) {
					//plus 6 hours, as the time table assumes starting time at 6am
					$hour = $i + 6;

					//setup rows and cells
					$table .= "<tr>";

					if($j%2==0) {
						$table .= "<td>$hour:00</td>";
					}
					else {
						$table .= "<td>&nbsp</td>"; //"<td>$hour:30</td>";
					}

					//setup div in cell with id format (hour).(min 0-30/31-59).(day of week)
					for($k=0; $k<7; $k++) {
						$id = $i + $j;
						if($j%2==0) {
							$table .= "<td bgColor=#EEEEEE><div id=\"$i.$j.$k\"></div></td>";
						}
						else {
							$table .= "<td class='border_bottom' bgColor=#EEEEEE><div id=\"$i.$j.$k\"></div></td>";
						}
						
					}
					$table .= "</tr>";
				}
			}

			//end of table
			$table .= "</table>";

			//display time table
			echo $table;
		?>

		<?php
			//get content of time table for the requested user 
			$query = "select name, start_time, end_time, day_of_week, event_id from time_table where UID=? order by day_of_week, start_time;";
			$stmt = $mysql->prepare($query);
			$stmt->execute(array($user_id));
			$rows = $stmt->fetchAll();
			$number_of_rows = count($rows);
			//$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
		?>

		<!-- hyper link to create a new event and logout link -->
		<a class="myButton" href="new_event.php">Add New Class</a> <a class="myButton"href="logout.php">Logout</a>
	</body>

	<script type="text/javascript">

		//fetch info from db and store in javascript array
		var events = new Array();
		<?php
			//new string
			$cell = ""; 

			//loop through database query result and put inside array
			for($i=0; $i<$number_of_rows; $i++) {
				//echo '<p>'.$row['name'].'</p>';
				//create a new sub array [][]
				$cell .= 'events['.$i.']=new Array();';

				//store event info: name, starting time, ending time, day of week
				$cell .= 'events['.$i.'][0]="'.$rows[$i]['name'].'";';

				//split time into three cell for HH:MM:SS
				$cell .= 'events['.$i.'][1]="'.$rows[$i]['start_time'].'".split(":");';
				$cell .= 'events['.$i.'][2]="'.$rows[$i]['end_time'].'".split(":");';

				//get day of week
				$cell .= 'events['.$i.'][3]="'.$rows[$i]['day_of_week'].'";';

				//get event id
				$cell .= 'events['.$i.'][4]="'.$rows[$i]['event_id'].'";';
			}

			//show cell/javascript array creation
			echo $cell;
		?>

		//iterate through events, 18 hours per day from 6am to 0am
		for(i=0;i<18;i++) {

			//assume the day starts from 6am
			var hour = i + 6;
			
			var numColors = 7;
			var colorArray = ["#A9D0F5", "#A9A9F5", "#F5A9A9", "#F5D0A9", "#D0F5A9", "#A9F5BC", "#F5A9E1"];
			var cellColor = colorArray[Math.floor(Math.random()*numColors)];

			//start checking per weekday
			for(k=0;k<7;k++) {
				//7 day per week
				for(l=0;l<<?php echo $number_of_rows; //loop through all events ?>;l++) {
					//console.log("hour: "+parseInt(events[l][2][0]));
					//console.log("minute: "+parseInt(events[l][2][1]));
					//console.log("name: "+events[l][0]);

					//check if event occur at that first half hour and week day match value i
					if(hour == parseInt(events[l][1][0]) && parseInt(events[l][1][1]) < 30 && parseInt(events[l][3]) == k) {
						
						//mark all the cells which spands through the duration of event
						var next_hour = i;	/* mark down current current hour */

						//check if the ending time has reached
						while((next_hour+6)<=parseInt(events[l][2][0]) && parseInt(events[l][2][1])<60) {

							for(x=1; x>=0; x--) {
								//console.debug("next_hour"+next_hour+":"+parseInt(events[l][2][0])+":"+parseInt(events[l][2][1])); 
								//break of last 30 min section
								//if ending time reached, break from while loop
								if((next_hour+6) == parseInt(events[l][2][0]) && parseInt(events[l][2][1]) <= 30 && x==1) {
									console.log("break");
									break;
								}
                                //mark the first cell with event name and info, apply styling here
								console.log(next_hour+" ");
								document.getElementById(next_hour+"."+x+"."+k).innerHTML = "<div id="+next_hour+"."+x+"."+k+" onclick=\"window.open('event.php?id="+events[l][4]+"','event','width=300,height=150');\">&nbsp</div>";

								document.getElementById(next_hour+"."+x+"."+k).style.backgroundColor = cellColor;
								//document.getElementById(next_hour+"."+x+"."+k).innerHTML = "<div id="+next_hour+"."+x+"."+k+">"+next_hour+"."+x+"."+k+"</div>";
							}
							next_hour++;
						}
						//color
						document.getElementById(i+".0."+k).style.backgroundColor = cellColor;
						document.getElementById(i+".0."+k).innerHTML = "<div id="+i+".0."+k+"><a target=\"popup\" onclick=\"window.open('event.php?id="+events[l][4]+"','event','width=300,height=150');\"><b>"+events[l][0]+"</b></a></div>";
					}
					//check if event occur at the next half hour
					else if(hour == parseInt(events[l][1][0]) && parseInt(events[l][1][1]) >= 30 && parseInt(events[l][3]) == k) {
						
						//marking the cells which span through the event duration
						var next_hour = i + 1;
						while((next_hour+6)<=parseInt(events[l][2][0]) && parseInt(events[l][2][1])<60) {
							//break if reaching last block

							for(x=0; x<2; x++) {
								//console.debug("next_hour"+next_hour+":"+parseInt(events[l][2][0])+":"+parseInt(events[l][2][1])); 
								//break
								if((next_hour+6) == parseInt(events[l][2][0]) && parseInt(events[l][2][1]) <= 30 && x==1) {
									//console.log(events[l][0]+" "+events[l][2][0]+":"+events[l][2][1]);
									//console.log("lower "+x);
									break;
								}
								//color
								document.getElementById(next_hour+"."+x+"."+k).style.backgroundColor = cellColor;
								document.getElementById(next_hour+"."+x+"."+k).innerHTML = "<div id="+next_hour+"."+x+"."+k+" onclick=\"window.open('event.php?id="+events[l][4]+"','event','width=300,height=150');\">&nbsp</div>";
							}
							next_hour++;
						}
						//color
						document.getElementById(i+".1."+k).style.backgroundColor = cellColor;
						document.getElementById(i+".1."+k).innerHTML = "<div id="+i+".0."+k+"><a target=\"popup\" onclick=\"window.open('event.php?id="+events[l][4]+"','event','width=300,height=150');\"><b>"+events[l][0]+"</b></a></div>";
					}
				}
			}
		}

	</script>

</html>
