<!--
Copyright (C) 2014 Steven Chien, Troy Lenger and Nikita Ko

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->

<?php
	//backup only!
	function getSchedule($mysql,$user_name,$start_time,$end_time,$day_of_week) {
		try {
			$query = "select name, start_time, end_time from time_table where UID=? and day_of_week=? and start_time>=? and start_time<=? order by start_time";
			$exec = $mysql->prepare($query);
			$exec->execute(array($user_name,$day_of_week,$start_time,$end_time));
			$exec->setFetchMode(PDO::FETCH_ASSOC);
			$rows = $exec->fetchAll();

			return $rows;
		}
		catch(PDOexception $e) {
			$e->getMessage();
		}
	}
?>
