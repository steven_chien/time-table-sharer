<!DOCTYPE html>
<!--
Copyright (C) 2014 Steven Chien, Troy Lenger and Nikita Ko

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->
<?php
        // put your code here
	if(isset($_COOKIE['time_table_sharer_login'])&&isset($_COOKIE['user_name'])) {
		header("Location:time_table.php");
	}
?>
<html>
	<head>
		<meta charset="UTF-8">
		<title>login - Time Table Viewer</title>
		<link rel="stylesheet" type="text/css" href="index.css"></link>
	</head>
	<body>
		<div>
		<form name="login_details" method="post" action="login.php">
			<h1>Timetable Viewer</h1>
			<h4>Please login in,</h4>
			Email: <input type="email" name="Email" required>
			Password:<input type="password" name="Password" required>
			<input class="myButton" type="submit" value="login" name="login">
		</form>
		<p><a class="myButton" href="new_account.php">New Account</a></p>
		</div>
	</body>
</html>