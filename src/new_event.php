<!DOCTYPE html>
<!--
Copyright (C) 2014 Steven Chien, Troy Lenger and Nikita Ko

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->

<?php
	//check login status and setup database connection then setup environment variable
	include("check_login.php");
	include("database.php");
	extract($_COOKIE);

	//check if data is posted
	if(isset($_POST['submit'])) {
		extract($_POST);

		try {
			//if data posted, insert into table
			$query = "insert into time_table(name, start_time, end_time, day_of_week, UID, location) values(?,?,?,?,?,?);";

			//check if course code is set, if so get data from class info
			if(empty($course_code)) {
				//event name is selected by user
				$event = $event_name;

				//error checking
				if($starting>$ending || $starting<'06:00:00' || (empty($name)||empty($starting)||empty($ending))) {
					die("wrong input!");

				}


			}
			else {
				//if course code is set, get info from database and insert into time table
				$event = $course_code;

				$get_info_query = "select class_id, class_name, start_time, end_time, day_of_week, location from class_info where class_id=?;";
				$stmt = $mysql->prepare($get_info_query);
				$stmt->execute(array($event));
				$row = $stmt->fetchAll(PDO::FETCH_ASSOC);
				$row_count = count($row);
				
				if($row_count==0) {
					die("class not found<br><a href=\"time_table.php\">back</a>");
				}

				$event = $row[0]['class_id'];
				$starting = $row[0]['start_time'];
				$ending = $row[0]['end_time'];
				$weekday = $row[0]['day_of_week'];
				$location = $row[0]['location'];
			}

			//check collision
			$testQuery = "select * from time_table where (((start_time>? and start_time<?) or (end_time>? and end_time<?)) and day_of_week=?) and UID=?";
			$stmt = $mysql->prepare($testQuery);
			$stmt->execute(array($starting, $ending, $starting, $ending, $weekday, $user_name));
			$rows = $stmt->fetchAll();
			$row_count = count($rows);

			//execute insertion if no collision
			if($row_count==0) {
				$exec = $mysql->prepare($query);
				$exec->execute(array($event,$starting,$ending,$weekday,$user_name,$location));
				header("Location:index.php");
			}
			else {
				die("event collision<br><a href=\"time_table.php\">back</a>");
			}
		}
		catch(PDOexception $e) {
			echo $e->getMessage();
		}
	}
?>
<html>
<head>
	<title>New Event - Time Table Viewer</title>
	<link rel="stylesheet" type="text/css" href="index.css">
</head>
<body>
	<form name="new_event" method="post" action="new_event.php">

		<h3>Automatically add all classes for a specific course</h3>
		<p style="text-indent:2.0em">Course Code: <input type="text" name="course_code"></p>
		
		<h3>Or create a new class,</h3>
		
		<p style="text-indent:2.0em">Event Name:<input type="text" name="event_name" ></p>
		<p style="text-indent:2.0em">Start Time:<input type="time" name="starting" ></p>
		<p style="text-indent:2.0em">End Time:<input type="time" name="ending" ></p>
		<p style="text-indent:2.0em">Location:<input type="text" name="location"></p>
		<p style="text-indent:2.0em">Day of Week:
		<select name="weekday" >
			<option value="0">Sun</option>
			<option value="1">Mon</option>
			<option value="2">Tue</option>
			<option value="3">Wed</option>
			<option value="4">Thu</option>
			<option value="5">Fri</option>
			<option value="6">Sat</option>
		</select></p><br>
		<input class="myButton" type="submit" name="submit" value="submit">
	</form>
</body>
</html>
