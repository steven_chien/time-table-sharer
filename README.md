##Introduction##

The system is a website for users to up load their own time table and view others' time table at the same time.
It consists of a MySQL server and a web server with PHP installed.

The System is developed for The Hong Kong Polytechnic University COMP320 Introduction to Internet Computing, Semester 2, Spring 2014

##File structure##

	- src/delete.php ....................... To delete events
	- src/event.php ........................ To show a pop up windows populated with detail event info.
	- src/database.php ..................... To establish connect to the database server, connection named $mysql
	- src/check_login.php .................. To validate login status by checking the user's cookies
	- src/friends.php ...................... To process of requesting to add a particular user ID to the current user's friend list
	- src/time_table.php .................... The main page to display requested time table
	- src/new_event.php ..................... To process the request to add a new event
	- src/login.php ......................... Validate user login and if success create cookies on the user's computer
	- src/logout.php ........................ Destroy cookies on user's computer
	- src/index.php ......................... Login page
	- doc/COMP320 Term Project Report.pdf ... Project Report

##License##

Copyright (C) 2014 Steven Chien, Troy Lenger et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
